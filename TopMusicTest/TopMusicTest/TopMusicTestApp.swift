//
//  TopMusicTestApp.swift
//  TopMusicTest
//
//  Created by Moreno Nicolelli on 24/08/21.
//

import SwiftUI

@main
struct TopMusicTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
